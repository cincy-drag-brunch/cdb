(function($) {
    "use strict";
    
	/* STICKY NAVIGATION */
    $(window).scroll(function() {
            var windowsize = $( window ).width();
            if (windowsize >= 1600 && $(this).scrollTop() >= 346) {
                $('.header').addClass("sticky");
            }
            else if((windowsize >= 1200 && windowsize < 1600) && $(this).scrollTop() >= 230)
            {
                $('.header').addClass("sticky");
            }
            else if((windowsize >= 992 && windowsize < 1200) && $(this).scrollTop() >= 200)
            {
                $('.header').addClass("sticky");
            }
            else {
                $('.header').removeClass("sticky");
            }

        });
    
    /* Go up */
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 800) {
            jQuery(".go-up").css("bottom", "0");
        } else {
            jQuery(".go-up").css("bottom", "-60px");
        }
    });
    jQuery(".go-up").click(function() {
        jQuery("html,body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });   
    
})(jQuery);


    /* WOW Animation */  
    wow = new WOW({
        mobile:true,
    })
    wow.init();
    